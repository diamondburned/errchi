package main

import (
	"fmt"
	"log"
	"net/http"

	errchi ".."
)

func TestMiddleware(next errchi.Handler) errchi.Handler {
	return errchi.HandlerFunc(func(w http.ResponseWriter, r *http.Request) (int, error) {
		fmt.Println("Worked!")
		w.Write([]byte("Hello!\n"))

		return next.ServeHTTP(w, r)
	})
}

func NormalMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("Normal middleware also worked!")
		w.Write([]byte("Hello again!\n"))

		next.ServeHTTP(w, r)
	})
}

func main() {
	r := errchi.NewRouter()
	r.Use(TestMiddleware)
	r.Router.Use(NormalMiddleware)

	r.Get("/", func(w http.ResponseWriter, r *http.Request) (int, error) {
		w.Write([]byte(r.Host))
		return 200, nil
	})

	log.Println("Listening at :6869")
	if err := http.ListenAndServe(":6869", r); err != nil {
		panic(err)
	}
}
