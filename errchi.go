package errchi

import (
	"net/http"

	"github.com/go-chi/chi"
)

type MiddlewareFunc func(Handler) Handler

type HandlerFunc func(http.ResponseWriter, *http.Request) (int, error)

func (f HandlerFunc) ServeHTTP(w http.ResponseWriter, r *http.Request) (int, error) {
	return f(w, r)
}

type Handler interface {
	ServeHTTP(http.ResponseWriter, *http.Request) (int, error)
}

type Router struct {
	chi.Router
	middlewares []MiddlewareFunc
}

var _ http.Handler = (*Router)(nil)

func NewRouter() *Router {
	return &Router{chi.NewRouter(), nil}
}

// Route mounts a sub-Router along a `route`` string.
func (r *Router) Route(route string, c func(*Router)) {
	r.Router.Route(route, func(chiRouter chi.Router) {
		c(&Router{
			Router:      chiRouter,
			middlewares: append([]MiddlewareFunc{}, r.middlewares...), // copy
		})
	})
}

// Use appends one or more middlewares onto the Router stack.
func (r *Router) Use(ms ...MiddlewareFunc) {
	r.middlewares = append(r.middlewares, ms...)
}

// UseRegular appends regular HTTP middlewares into the underlying router.
func (r *Router) UseRegular(ms ...func(http.Handler) http.Handler) {
	r.Router.Use(ms...)
}

// With adds inline middlewares for an endpoint handler.
func (r *Router) With(ms ...MiddlewareFunc) *Router {
	// Copy the router
	r = &(*r)
	r.middlewares = append(r.middlewares, ms...)
	return r
}

func (r *Router) Connect(route string, hf HandlerFunc) {
	r.Router.Connect(route, r.handlerfunc(hf))
}

func (r *Router) Delete(route string, hf HandlerFunc) {
	r.Router.Delete(route, r.handlerfunc(hf))
}

func (r *Router) Get(route string, hf HandlerFunc) {
	r.Router.Get(route, r.handlerfunc(hf))
}

func (r *Router) Head(route string, hf HandlerFunc) {
	r.Router.Head(route, r.handlerfunc(hf))
}

func (r *Router) Options(route string, hf HandlerFunc) {
	r.Router.Options(route, r.handlerfunc(hf))
}

func (r *Router) Patch(route string, hf HandlerFunc) {
	r.Router.Patch(route, r.handlerfunc(hf))
}

func (r *Router) Post(route string, hf HandlerFunc) {
	r.Router.Post(route, r.handlerfunc(hf))
}

func (r *Router) Put(route string, hf HandlerFunc) {
	r.Router.Put(route, r.handlerfunc(hf))
}

func (r *Router) Trace(route string, hf HandlerFunc) {
	r.Router.Trace(route, r.handlerfunc(hf))
}

func (r *Router) HandleFunc(route string, hf HandlerFunc) {
	r.Router.HandleFunc(route, r.handlerfunc(hf))
}

func (r *Router) MethodFunc(method, route string, hf HandlerFunc) {
	r.Router.MethodFunc(method, route, r.handlerfunc(hf))
}

func (r *Router) chain(endpoint Handler) Handler {
	if len(r.middlewares) == 0 {
		return endpoint
	}

	h := r.middlewares[len(r.middlewares)-1](endpoint)
	for i := len(r.middlewares) - 2; i >= 0; i-- {
		h = r.middlewares[i](h)
	}

	return h
}

func (r *Router) serveChi(wr http.ResponseWriter, rq *http.Request) (int, error) {
	r.Router.ServeHTTP(wr, rq)
	return 0, nil
}

func (r *Router) ServeHTTP(wr http.ResponseWriter, rq *http.Request) {
	var handler Handler = HandlerFunc(r.serveChi)
	for _, mw := range r.middlewares {
		handler = mw(handler)
	}

	r.handler(handler).ServeHTTP(wr, rq)
}

/* LMAOOOOO
func (r *Router) middleware(m func(Handler) Handler) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return r.handlerfunc(HandlerFunc(func(w http.ResponseWriter, r *http.Request) (int, error) {
			return m(HandlerFunc(func(w http.ResponseWriter, r *http.Request) (int, error) {
				next.ServeHTTP(w, r)
				return 200, nil
			})).ServeHTTP(w, r)
		}))
	}
}
*/

func wrap(theirs http.Handler) HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) (int, error) {
		theirs.ServeHTTP(w, r)
		return 0, nil
	}
}

func (r *Router) handler(h Handler) http.Handler {
	return r.handlerfunc(h.ServeHTTP)
}

func (router *Router) handlerfunc(hf HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c, err := router.chain(hf).ServeHTTP(w, r)
		if err != nil {
			if c == 0 {
				c = 500
			}

			w.WriteHeader(c)
			ErrorWriter(w, err)

			return
		}

		switch c {
		case 0, 200:
			// 0   - special case, ignore
			// 200 - Go will implicitly write this
			return
		default:
			// If the returned code is not 200, meaning w.Write is likely not
			// called, we write that header
			w.WriteHeader(c)
		}
	}
}
