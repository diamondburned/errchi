package errchi

import (
	"encoding/json"
	"net/http"
)

type Error struct {
	Error string `json:"error"`
}

var errCrafting, _ = json.Marshal(Error{
	Error: "Failed to craft JSON error",
})

func CraftJSON(err error) []byte {
	j, err := json.Marshal(Error{
		Error: err.Error(),
	})

	if err != nil {
		return errCrafting
	}

	return j
}

var ErrorWriter = WriterJSON

func WriterJSON(w http.ResponseWriter, err error) {
	w.Write(CraftJSON(err))
}
